@extends('layouts.master')
    @section('content')

    <section class="about-section">
		<div class="about-warp">
			<div class="about-left">
				<div class="about-img">
					<img src="{{asset('avatar/'.$profile->gambar_profil)}}" alt="">
				</div>
				<div class="profile-text text-white">
					<h2>Hello!</h2>
					<h2>Saya <strong>{{$user->nama}}</strong></h2>
				</div>
			</div>
			<div class="about-right">
				<div class="about-text">
					<h2>Edit Profile</h2>
				</div>
				<div class="skill-warp text-center  mt-1">
					<form>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Profile</label>
                          <input type="text" class="form-control" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Bio</label>
                            <textarea class="form-control" rows="3" name="bio"></textarea>
                          </div>
                            <div class="form-group">
                              <label for="exampleFormControlFile1">Foto Profil</label>
                              <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                        <button type="submit" class="btn btn-primary">Update Profile</button>
                      </form>
				</div>
			</div>
		</div>
	</section>
	

@endsection