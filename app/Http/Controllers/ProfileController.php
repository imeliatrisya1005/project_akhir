<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $profile = Profile::where('user_id',$id)->first();
        $post = Post::where('user_id',$id)->get();
        return view('layouts.index', compact(['profile','post']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::where('user_id',$id)->first();
        $post = Post::where('user_id',$id)->get();
        return view('layouts.index', compact(['users']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::where('user_id',$id)->first();
        
        return view('layouts.index', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'gambar_profil' => 'mimes:jpg,png,jpeg',
        ]);

        $profile = Profile::where('user_id',$id)->first();

        if($request->has('gambar_profil')){
            
            if($profile->gambar_profil!="ava.png"){
                $path = "avatar/";
                File::delete($path.$profile->gambar_profil);
            }

            $fileName = 'IMG'.'-'.time().'.'.$request->gambar_profil->extension();
            $request->gambar_profil->move(public_path('avatar'),$fileName);

            $profile_data = [
                'gambar_profil'=>$fileName,
                'bio'=>$request->bio,
            ];

        } else {
            $profile_data = [
                'bio'=>$request->bio,
            ];
        }
        
        $profile->update($profile_data);

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

